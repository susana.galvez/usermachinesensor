import {DataTypes} from "sequelize";
import { sequelize } from "../database/database.js";

const MachinesModel= sequelize.define('machines',{
    
    ref:{
        type:DataTypes.STRING,
        primaryKey: true,
        allowNull: false
    },

    name:{
        type:DataTypes.STRING,
        allowNull: false
    },

    brand:{
        type:DataTypes.STRING,
         
    },

    model:{
        type:DataTypes.STRING
    },

    startDate:{
        type:DataTypes.DATE,
        allowNull: false
    },

    endDate:{
        type:DataTypes.DATE,
    },

    deregistered:{
        type:DataTypes.BOOLEAN,
        //falta validar true si endDate no es nulo
    },

    heads:{
        type:DataTypes.INTEGER,//minimo 1,máximo 5
        allowNull: false,
        validate:{len:{args:[1,5]}}//no sé si está bién
    },
    status:{
        type:DataTypes.STRING,//(‘stopped’,‘working’,‘paused’,‘maintenance’) 
        allowNull: false
    },

    powerKw:{
        type:DataTypes.FLOAT,
        allowNull: false
    },

    emailAlerts:{
        type:DataTypes.BOOLEAN,//por defecto false
        allowNull: false
    },



},{timestamps:false})


export default MachinesModel;