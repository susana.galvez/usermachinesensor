import {DataTypes} from "sequelize";
import { sequelize } from "../database/database.js";

const SensorsModel= sequelize.define('sensors',{
    code:{
        type:DataTypes.STRING,
        primaryKey: true,
        
    },

    nameSensor:{
        type:DataTypes.STRING,
        allowNull: false
    },

    brandSensor:{
        type:DataTypes.STRING,
         
    },

    modelSensor:{
        type:DataTypes.STRING
    },

    type:{
        type:DataTypes.STRING,
        allowNull: false
    },

    startDate:{
        type:DataTypes.DATE,
        allowNull: false
    },

    endDate:{
        type:DataTypes.DATE,

    },

    deregisteredSensor:{
        type:DataTypes.BOOLEAN,
        //falta validar true si endDate no es nulo
    },

    heads:{
        type:DataTypes.INTEGER,//minimo 1,máximo 5
        allowNull: false
    },
    status:{
        type:DataTypes.STRING,//(‘stopped’,‘working’,‘paused’,‘maintenance’) 
        allowNull: false
    },

    powerKw:{
        type:DataTypes.FLOAT,
        allowNull: false
    },

    emailAlerts:{
        type:DataTypes.BOOLEAN,//por defecto false
        allowNull: false
    }

    

},{timestamps:false})


export default SensorsModel;;