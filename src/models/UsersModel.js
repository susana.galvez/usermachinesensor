
"use strict";
const DataTypes = require ('sequelize');
const sequelize = require('../database/database.js');


const UsersModel = sequelize.define('users', {
    
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    surnames: {
        type: DataTypes.STRING,
        allowNull: false
    },

    userName: {
        type: DataTypes.STRING, 
        primaryKey: true,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING, 
        allowNull: false, 
        unique: true, 
        // validate: { notNull, isEmail }
     },

    password:{
        type: DataTypes.STRING ,
        allowNull: false,
        // validate: { notNull }
        
    } 
    
}, { timestamps: true })



module.export = { UsersModel };