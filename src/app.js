const express= require('express');
//  import morgan from 'morgan'
 const app = express();

// Importing route
const usersRoutes = require('./routes/userRoutes.js')  ;

// Middlewares
// app.use(morgan('dev'));
app.use(express.json());

// routes
 app.use('/api/users',usersRoutes);
//  app.use(machinesRoutes);


module.exports= app;