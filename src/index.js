const app = require('./app.js');
const sequelize = require('./database/database.js');


async function  main(){
    try{
        await sequelize.sync({force:false})
        await sequelize.authenticate();
        console.log("Conection has been established successfully");
        app.listen(4000);
        console.log("Server is listening on port 4000",4000)
    }catch(error){
        console.log("No puedes conectarte",error)
    }
}
main();

